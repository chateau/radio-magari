const $$ = require("squeak/node");

// get electrode lib
let electrodeBooter = require("electrode/boot");
// get config
let config = require("./config");

try {
  // try to import the file `config/public.js`
  require("../config/public");
  // start app
  electrodeBooter(config);
}
catch (e) {
  // `config/public.js` is missing, do not start and display error
  $$.log.error("Your app '"+ config.name +"' cannot start if you didn't create the file `config/public.js`, see readme for more details!");
};
