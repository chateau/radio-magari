var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  initialize: function () {
    var radio = this;

    // CREATE MENU BUTTON
    uify.button({
      $container: radio.$el,
      icomoon: "lines",
      class: "radio-menu_button",
      click: function () { radio.$el.toggleClass("menu-visible"); },
    });

    // TITLE
    radio.menu.$title = radio.menu.$el.img({
      class: "title",
      src: "/radio-assets/menu/title.png",
    }).click(function () {
      radio.open("home");
    });

    // PAGES
    radio.menu.$pagesList = radio.menu.$el.div({ class: "pages_list", });

    // LOCALE SWITCHER
    radio.menu.$localeSwitcher = radio.menu.$el.div({ class: "locale_switcher", });

    // POPULATE PAGES AND LOCALE SWITCHER
    radio.menu.refresh();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REFRESH
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  refresh: function () {
    var radio = this;

    // empty pages list container
    radio.menu.$pagesList.empty();
    // (re)create list of pages
    _.each(radio.config.pages, function (pageName) {
      radio.menu.$pagesList.img({
        "page-name": pageName,
        src: "/radio-assets/menu/page-"+ radio.localeCode +"/"+ pageName +".png",
      }).click(function () {
        radio.open(pageName);
      });
    });

    // empty pages list container
    radio.menu.$localeSwitcher.empty();
    // refresh locale buttons
    if (radio.config.availableLocales.length > 1) _.each(radio.config.availableLocales, function (localeCode) {
      var $localeCode = radio.menu.$localeSwitcher.div({ text: localeCode }).click(function () {
        radio.changeLocale(localeCode);
      });
      if (localeCode === radio.localeCode) $localeCode.addClass("current");
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
