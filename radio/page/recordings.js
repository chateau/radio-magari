var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/date");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SORT AND DISPLAY LIST OF PODCASTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function sortAndDisplayPodcasts (radio) {

  // SORTING
  // older to recent
  if (radio.page.recordings.currentSorting === "dateAsc") var videosList = _.clone(radio.page.recordings.videosList).reverse()
  // recent to old
  else var videosList = radio.page.recordings.videosList;

  // EMPTY CONTENTS
  radio.page.recordings.$contents.empty();

  // DISPLAY CONTENTS GROUPED
  if (radio.page.recordings.currentGrouping) {
    var videosGrouped = _.groupBy(videosList, function (videoObject) {
      return videoObject.channel.displayName
    });
    _.each(videosGrouped, function (videosList, groupName) {
      radio.page.recordings.$contents.div({
        class: "podcast-group-title",
        text: groupName,
      });
      _.each(videosList, function (videoObject) {
        podcastDisplay(videoObject, radio);
      });
    });
  }
  // // DISPLAY CONTENTS NON GROUPED
  else _.each(videosList, function (videoObject) {
    podcastDisplay(videoObject, radio);
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE PODCAST DISPLAY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function podcastDisplay (videoObject, radio) {

  // CREATE PODCAST CONTAINER
  var $podcast = radio.page.recordings.$contents.div({ class: "podcast", });

  // IMAGE
  var $preview = $podcast.div({
    class: "podcast-preview",
  }).click(function () {
    radio.player.load(videoObject);
  }).css("background-image", "url('"+ radio.config.peertubeInstance + videoObject.previewPath +"')");

  // OVERLAY
  var $previewOverlay = $preview.div({ class: "podcast-preview-overlay", });
  $previewOverlay.div({ class: "podcast-preview-overlay-icon icon-play3", });

  // TIME
  var $previewPodcastTime = $preview.div({
    class: "podcast-preview-time",
    text: radio.player.makeTimeDisplay(videoObject.duration),
  });
  console.log(videoObject);

  // DETAILS
  var $details = $podcast.div({ class: "podcast-details", });
  $details.div({
    class: "podcast-program",
    text: videoObject.channel.displayName,
  });
  $details.div({
    class: "podcast-date",
    text: $$.date.moment(new Date(videoObject.originallyPublishedAt)).format("YYYY-MM-DD"),
  });

  // TITLE
  $podcast.div({
    class: "podcast-title",
    text: videoObject.name,
  }).click(function () {
    radio.player.load(videoObject);
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE FILTERING BUTTONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function filteringButtons ($filters, radio) {

  // DEFAULT SORTING ORDER
  radio.page.recordings.currentSorting = "dateDesc";
  radio.page.recordings.currentGrouping = false;

  // SORT BY DATE
  var $sorting = $filters.div({
    class: "sorting-button",
    text: radio.locale.recordings.sort,
  }).click(function () {
    if (radio.page.recordings.currentSorting === "dateDesc") radio.page.recordings.currentSorting = "dateAsc"
    else radio.page.recordings.currentSorting = "dateDesc";
    $sorting.attr("current-sorting", radio.page.recordings.currentSorting);
    sortAndDisplayPodcasts(radio);
  });
  $sorting.attr("current-sorting", radio.page.recordings.currentSorting);

  // SORT BY PROGRAM
  var $grouping = $filters.div({
    class: "grouping-button",
    text: radio.locale.recordings.group,
  }).click(function () {
    radio.page.recordings.currentGrouping = !radio.page.recordings.currentGrouping;
    $grouping.attr("current-grouping", radio.page.recordings.currentGrouping);
    sortAndDisplayPodcasts(radio);
  });
  $grouping.attr("current-grouping", radio.page.recordings.currentGrouping);

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function () {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  var radio = this;

  // TITLE
  radio.$page.div({ class: "page-title", text: radio.locale.pageTitle.recordings, });

  // FILTER/SORTING
  var $filters = radio.$page.div({ class: "filters", });
  filteringButtons($filters, radio);

  // CONTENTS
  radio.page.recordings.$contents = radio.$page.div({ class: "contents", });
  var loadingSpinner = uify.spinner({
    $container: radio.page.recordings.$contents,
    text: radio.locale.recordings.loading +"...",
  });
  radio.peertube.listRadioVideos().then(function (videosList) {
    radio.page.recordings.videosList = videosList;
    sortAndDisplayPodcasts(radio);
    loadingSpinner.destroy();
  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
