var _ = require("underscore");
var $$ = require("squeak");
var noUiSlider = require("nouislider");
var keyboardify = require("keyboardify");
var uify = require("uify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  initialize: function () {
    var radio = this;

    radio.player.buttons();
    radio.player.title();
    radio.player.timeline();
    radio.player.$hiddenVideoContainer = radio.player.$el.div({ class: "radio-player-hidden_video_container", });
    radio.player.keyboardShortcuts();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  KEYBOARD SHORTCUTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  keyboardShortcuts: function () {
    var radio = this;

    keyboardify.bind("space", function (e) {
      if (!radio.player.$hiddenVideo || !radio.player.$hiddenVideo[0]) return;
      if (radio.player.currentlyPlaying) radio.player.pause()
      else radio.player.play();
      e.preventDefault();
    });

    keyboardify.bind("left", function () {
      if (!radio.player.$hiddenVideo || !radio.player.$hiddenVideo[0]) return;
      radio.player.changeTime(radio.player.$hiddenVideo[0].currentTime - 20);
    });

    keyboardify.bind("right", function () {
      if (!radio.player.$hiddenVideo || !radio.player.$hiddenVideo[0]) return;
      radio.player.changeTime(radio.player.$hiddenVideo[0].currentTime + 20);
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LOAD A PROGRAM
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  load: function (videoObject) {
    var radio = this;

    // load program image as thumbnail
    radio.player.$playpause.css("background-image", "url('"+ radio.config.peertubeInstance + videoObject.previewPath +"')");

    // load title and description
    radio.player.$programTitle.text(videoObject.name);
    radio.player.$programDescription.text($$.date.moment(new Date(videoObject.originallyPublishedAt)).format("YYYY-MM-DD"));

    // create time slider
    var $sliderContaier = radio.player.$timesliderContainer.empty().div();
    radio.player.timeslider = noUiSlider.create($sliderContaier[0], {
      start: 0,
      connect: [true, false],
      step: 1,
      range: {
        min: 0,
        max: videoObject.duration,
      },
    });

    radio.player.timeslider.on("slide", function (value) { // "update" would also run it when set() is fired
      radio.player.updateTime(+value[0], videoObject.duration);
      radio.player.$hiddenVideo[0].currentTime = value;
    });

    // set time texts
    radio.player.updateTime(0, videoObject.duration);

    // load video
    radio.player.loadVideo(videoObject);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LOAD VIDEO
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  loadVideo: function (videoObject) {
    var radio = this;

    // pause any playing video that was being played
    if (radio.player.$hiddenVideo && radio.player.$hiddenVideo.length) radio.player.pause();

    // show sound level button
    radio.player.$buttonsStart.show();
    radio.player.$buttonsEnd.show();

    // fetch video from peertube
    radio.peertube.fetch("videos/"+ videoObject.uuid).then(function (response) {

      // use the smallest file
      var videoFile = _.min(response.files, function (videoObject) { return videoObject.size });

      // create hidden video element
      radio.player.$hiddenVideoContainer.empty();
      radio.player.$hiddenVideo = radio.player.$hiddenVideoContainer.video();
      radio.player.$hiddenVideo.source({ src: videoFile.fileUrl, });

      // set desired video volume
      radio.player.$hiddenVideo[0].volume = radio.player.volumeslider.get();

    }).catch(function (err) {
      uify.toast.error("Failed to load video "+ videoObject.name +"<br>see logs for more details");
      $$.log.error("Failed to load video "+ videoObject.name);
      $$.log.error(err);
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  PLAY/PAUSE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  play: function () {
    var radio = this;

    radio.player.$hiddenVideo[0].play();
    radio.player.$playpauseIcon.removeClass("icon-play3").addClass("icon-pause2");

    // every second update text and sliders
    radio.player.currentlyPlayingInterval = setInterval(function () {

      // update time texts and slider
      radio.player.updateTime(
        Math.round(radio.player.$hiddenVideo[0].currentTime),
        Math.round(radio.player.$hiddenVideo[0].duration)
      );
      radio.player.timeslider.set(Math.round(radio.player.$hiddenVideo[0].currentTime));

      // if end of video, set to pause automatically
      if (radio.player.$hiddenVideo[0].currentTime == radio.player.$hiddenVideo[0].duration) radio.player.pause();

    }, 1000);

    radio.player.currentlyPlaying = true;

  },

  pause: function () {
    var radio = this;

    radio.player.$hiddenVideo[0].pause();
    radio.player.$playpauseIcon.removeClass("icon-pause2").addClass("icon-play3");
    clearInterval(radio.player.currentlyPlayingInterval);
    radio.player.currentlyPlaying = false;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHANGE TIME
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  changeTime: function (newTime) {
    var radio = this;

    // make sure newTime is not too low or high
    if (newTime < 0) newTime = 0;
    else if (newTime > radio.player.$hiddenVideo[0].duration) newTime = radio.player.$hiddenVideo[0].duration;

    // set time in video, slider, and text
    radio.player.$hiddenVideo[0].currentTime = newTime;
    radio.player.updateTime(
      Math.round(newTime),
      Math.round(radio.player.$hiddenVideo[0].duration)
    );
    radio.player.timeslider.set(Math.round(radio.player.$hiddenVideo[0].currentTime));

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  UPDATE TIME
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  updateTime: function (currentTime, duration) {
    var radio = this;

    radio.player.$timeline_passedTime.text(radio.player.makeTimeDisplay(currentTime));
    radio.player.$timeline_remaidingTime.text("-"+ radio.player.makeTimeDisplay(duration - currentTime));

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  BUTTONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  buttons: function () {
    var radio = this;

    //
    //                              PLAY/PAUSE BUTTON

    // start buttons container
    radio.player.$buttonsStart = radio.player.$el.div({ class: "radio-player-buttons-start", });

    // play/pause button and program thumbnail
    radio.player.$playpause = radio.player.$buttonsStart.div({
      class: "radio-player-buttons-playpause",
    }).click(function () {
      if (radio.player.currentlyPlaying) radio.player.pause()
      else radio.player.play();
    });

    radio.player.$playpauseIcon = radio.player.$playpause.div({
      class: "radio-player-buttons-playpause-icon icon-play3",
    });

    //
    //                              VOLUME BUTTON

    // end buttons container
    radio.player.$buttonsEnd = radio.player.$el.div({ class: "radio-player-buttons-end", });

    // play/pause button and program thumbnail
    radio.player.$soundlevel = radio.player.$buttonsEnd.div({ class: "radio-player-buttons-volume", });

    radio.player.$soundlevelIcon = radio.player.$soundlevel.div({
      class: "radio-player-buttons-volume-icon icon-volume-medium", // volume-mute2
    }).click(function (e) {
      radio.player.$soundlevelSlider.toggle();
      // do not propagat click event until global radio.$el container, otherwise it will trigger closing of slider just after opening it
      e.stopPropagation();
    });

    radio.player.$soundlevelSlider = radio.player.$soundlevel.div({
      class: "radio-player-buttons-volume-slider",
    }).click(function (e) {
      // make sure that clicking the slider doesn't close it
      e.stopPropagation();
    });
    radio.player.volumeslider = noUiSlider.create(radio.player.$soundlevelSlider[0], {
      start: 0.8,
      connect: [true, false],
      step: 0.01,
      direction: "rtl",
      orientation: "vertical",
      range: {
        min: 0,
        max: 1,
      },
    });

    radio.player.volumeslider.on("slide", function (value) { // "update" would also run it when set() is fired
      if (radio.player.$hiddenVideo && radio.player.$hiddenVideo.length) radio.player.$hiddenVideo[0].volume = value;
    });

    // hide slider when clicking anywhere
    radio.$el.click(function () {
      if (radio.player.$soundlevelSlider.css("display") !== "none") radio.player.$soundlevelSlider.hide();
    });

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  PROGRAM TITLE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  title: function () {
    var radio = this;

    // texts container
    radio.player.$titlesContainer = radio.player.$el.div({ class: "radio-player-title_container", });

    // title and description/date containers
    radio.player.$programTitle = radio.player.$titlesContainer.div({ class: "radio-player-title", });
    radio.player.$programDescription = radio.player.$titlesContainer.div({ class: "radio-player-description", });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  TIMELINE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  timeline: function () {
    var radio = this;

    // timeline container
    radio.player.$timeline = radio.player.$el.div({ class: "radio-player-timeline", });

    // timeline passed time
    radio.player.$timeline_passedTime = radio.player.$timeline.div({ class: "radio-player-timeline-passed", });
    // timeline slider
    radio.player.$timesliderContainer = radio.player.$timeline.div({ class: "radio-player-timeline-slider", });
    // timeline remaining time
    radio.player.$timeline_remaidingTime = radio.player.$timeline.div({ class: "radio-player-timeline-remaining", });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GENERATE A PROPER DISPLAY FOR THE GIVEN TIME IN SECONDS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: convert a time in seconds into a nicely displayed time
    ARGUMENTS: ( !length <integer> « time in seconds » )
    RETURN: <string>
  */
  makeTimeDisplay: function (length) {
    if (!length) length = 0;
    var hours = Math.trunc(length / 3600);
    var minutes = Math.trunc(length / 60) - (hours * 60);
    var seconds = length % 60;

    var resultString = "";

    // hours and minutes
    if (hours) {
      resultString += hours +":";
      resultString += (minutes+"").length < 2 ? "0"+ minutes : minutes;
      resultString += ":";
    }
    // only minutes
    else resultString += minutes +":";

    // add seconds
    resultString += (seconds+"").length < 2 ? "0"+ seconds : seconds;

    return resultString;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
