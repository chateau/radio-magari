var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/url");
var $ = require("yquerj");
var publicConfig = require("../config/public");
var locales = require("./locales");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE FULL CONFIG FROM DEFAULTS AND PUBLIC CONFIG
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var fullConfig = $$.defaults({

  // PAGES ORDER
  pages: [
    "live",
    "programs",
    "recordings",
    "contacts",
  ],

  // AVAILABLE LOCALES
  locales: locales,

}, publicConfig);

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var radio = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ATTACH CONFIG OBJECT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  config: fullConfig,

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZE SITE DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  initialize: function () {
    var radio = this;

    // SUPPORT BETTER SMALL DEVICES
    if ($$.isTouchDevice()) $("head").append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />');

    // FIGURE OUT LOCALE (defines radio.locale and radio.localeCode)
    radio.figureOutLocale();

    // INITIALIZE APP CONTAINERS
    radio.initializeContainers();

    // INITIALIZE PARTS
    radio.menu.initialize();
    radio.decoration.initialize();
    radio.player.initialize();

    // FIGURE OUT CURRENT PAGE
    var urlQuery = $$.url.query.getAsObject();
    radio.open(urlQuery.p || "home");

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CREATE CONTAINERS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  initializeContainers: function () {
    var radio = this;

    radio.$el = $app.div({ class: "radio", });
    radio.decoration.$el = radio.$el.div({ class: "radio-decoration", });
    radio.$page = radio.$el.div({ class: "radio-page", }).click(function (e) {
      // when menu is open, close it when clicking on page
      if (radio.$el.hasClass("menu-visible")) {
        radio.$el.removeClass("menu-visible");
        e.stopPropagation();
      };
    });
    radio.menu.$el = radio.$el.div({ class: "radio-menu", }); // on top of page (important for locale)
    radio.player.$el = radio.$el.div({ class: "radio-player", });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FIGURE OUT LOCALE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION:
      figure out current locale and locale code
      (checks query, then locale storage, then navigator config to get right locale code)
    ARGUMENTS: ( ø )
    RETURN: <string> « valid locale code (key of radio.config.locales) »
  */
  figureOutLocale: function () {
    var radio = this;

    // GET LOCALE FROM QUERY
    radio.localeCode = $$.url.query.getAsObject().l;
    radio.locale = radio.config.locales[radio.localeCode];
    if (radio.locale) return;

    // GET LOCALE FROM LOCAL STORAGE
    radio.localeCode = localStorage.getItem("localeCode");
    radio.locale = radio.config.locales[radio.localeCode];
    if (radio.locale) return;

    // GET LOCALE FROM BROWSER CONFIG
    var browserLanguage = window.navigator.userLanguage || window.navigator.language;
    // iterate supportedLanguages to see if browser language is supported
    for (var i = 0; i < radio.config.availableLocales.length; i++) {
      radio.localeCode = $$.match(browserLanguage, "^"+ radio.config.availableLocales[i]);
      radio.locale = radio.config.locales[radio.localeCode];
      if (radio.locale) return localStorage.setItem("localeCode", radio.localeCode);
    };

    // STILL NO LOCALE, USE THE FIRST IN LIST OF LOCALES
    radio.localeCode = radio.config.availableLocales[0];
    radio.locale = radio.config.locales[radio.localeCode];
    localStorage.setItem("localeCode", radio.localeCode);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHANGE LOCALE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: change current locale
    ARGUMENTS: ( !localeCode <string> )
    RETURN: <void>
  */
  changeLocale: function (localeCode) {
    var radio = this;

    // change locale
    radio.localeCode = localeCode;
    radio.locale = radio.config.locales[radio.localeCode];

    // set locale in query and localStorage
    $$.url.query.modify({ l: localeCode, });
    localStorage.setItem("localeCode", localeCode);

    // refresh menu and opened page
    radio.menu.refresh();
    radio.open(radio.currentPage);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  OPEN PAGE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  open: function (pageName) {
    var radio = this;

    // stop here if page asked doesn't exist
    if (!radio.page[pageName]) return $$.log.error("The page you asked to open ("+ pageName +") doesn't seem to exist!");

    // set current page to radio, dom and url query
    radio.currentPage = pageName;
    radio.$el.attr("current-page", pageName);
    $$.url.query.modify({ p: pageName, });

    // make sure that the right menu item is underlined
    radio.menu.$pagesList.children().removeClass("current");
    radio.menu.$pagesList.children("[page-name="+ pageName +"]").addClass("current");

    // change background image
    radio.decoration.setBackgroundImage();

    // hide menu (in case it was shown)
    radio.$el.removeClass("menu-visible");

    // create the corresponding page content
    radio.$page.empty();
    radio.page[pageName]();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

radio.menu = $$.methods({}, require("./menu"), { context: radio, });
radio.player = $$.methods({}, require("./player"), { context: radio, });
radio.decoration = $$.methods({}, require("./decoration"), { context: radio, });
radio.peertube = $$.methods({}, require("./peertube"), { context: radio, });
radio.page = {
  contacts:    _.bind(require("./page/contacts"),   radio),
  home:        _.bind(require("./page/home"),       radio),
  live:        _.bind(require("./page/live"),       radio),
  programs:    _.bind(require("./page/programs"),   radio),
  recordings:  _.bind(require("./page/recordings"), radio),
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = radio;
