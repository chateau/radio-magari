# radio-magari

The website for Radio Magari.
Built with electrode, it creates a nice way to display contents from peertube.


## Customize radio-magari to make your own site

You can easily make use of radio-magari to make your own radio site by just modifying a few variables and files.

1. Customize the images to use as home, program, menu to your own taste and site needs (you should replace the ones in the `public/radio-assets` directory with other corresponding images giving them the same name (and extension)). You can also replace the `public/share/favicon.png` (this is the icon displayed in the browser tab).
2. Create/customize `config/public.js` with values connecting to your own peertube instance and user:
For example:
```js
module.exports = {
  // title displayed in browser title bar
  siteTitle: "My radio",
  // the peertube instance where your data is stored
  peertubeInstance: "https://myPeertubeInstance.tld",
  // the peertube user of your radio (all it's contents will be shown on the website, sorted by channel(=programs)/tags...)
  peertubeUser: "someuser",
  // the languages the site is available in
  // available languages are english and italian, if you want other languages, you will need to customize the file "radio/locales.js", feel free to share your translations to integrate them directly in this repository
  availableLocales: ["it", "en"],
  // locales complements, contents of some pages
  en: {
    // contacts page text
    contacts: ["Text to display on contacts page."],
    // list of radio programs
    programs: [
      "- first program  | mondays at 6PM | by Abdula",
      "- second program | sundays at 3PM | by Rosy",
    ],
  },
  it {
    contacts: ["Testo visibile sulla pagina contatti."],
    programs: [
      "- primo programma   | lunedi alle 18   | da Abdula",
      "- secondo programma | domenica alle 15 | da Rosy",
    ],
  },
};
```
3. If you want you can also create a `config/private.js` to customize some other settings.
For example:
```js
module.exports = {
  // the name of your app as displayed when starting it
  name: "my-radio",
  // the port at which to run your site
  port: "8000",
  // if you want to rebuild pages at each request (conveninent for developpment)
  production: false,
  // to have more debugging logs
  debug: true,
};
```
4. If you want to customize the theme used for the app (= colors and styles), you can modify the `radio/styles/theme.less` file.
5. By default the site is made of four pages: live, programs, recordings and contacts. You can select which ones to display and in which order by just adding to your "config/public.js" file a "pages" key with the list of pages you want to see and your customized order. For example, it could look like this:
```js
pages: [
  "recordings",
  "programs",
  "contacts",
],
```
